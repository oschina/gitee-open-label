# Gitee 开放标签

### 💭介绍

这是一个Gitee用来展示站内精选的标签的项目，我们定期会在这里推荐优质、好玩的标签内容，同时也欢迎大家推荐有趣的标签，帮助好的内容获得曝光！

### ⭕快速开始

好的项目值得被曝光！

本项目中会定期更新一些Gitee站内的实用，好玩的标签链接，点开就可以查看所有该标签相关的项目。当然我们也欢迎大家为自己的项目打上合适、精准的标签，以便于更多的人看到你为开源所做的贡献。

#### 如何玩转标签

1、进入自己的【项目-代码】页

![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/135521_d7c3fd20_5370906.png "屏幕截图.png")

2、选择编辑【简介】

![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/135631_893abe9b_5370906.png "屏幕截图.png")

3、输入自己项目相关的标签并保存

![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/135652_79ae7ee0_5370906.png "屏幕截图.png")

4、点击感兴趣的标签，就会看到Gitee上所有关于这个标签的开源项目

![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/135804_fda25f30_5370906.png "屏幕截图.png")

### 🙋‍♂️内容不够？我来推荐！

如果你发现在项目中并没有你所感兴趣的标签，或者说当前推荐的标签里缺少了某些重要信息，非常欢迎使用上方的【issue】功能告诉我们。

同时，如果你在Gitee中发现（或者你创建了一个）好玩又能有益大众的标签，欢迎在这个仓库创建一个Pull Request，附上你对这个标签的推荐语，并把它的名字和链接告诉我们，该Pull Request审查合并后，我们将会把它加入这个项目。

请注意，所有建议都必须遵守Gitee的社区准则和服务条款。

推荐好的标签，就有机会获得Gitee 小礼品哦，Gitee鼠标垫，文化衫~ 成为程序猿里时尚弄潮儿 只要你推荐，每个月我们都会选出3位幸运推荐者，赠送礼品~

### 👍评选标准

在建议添加内容时，请考虑那个标签是有用并与大多数人相关，而不仅仅是服务于特定的用例。









